package example.com.redux;

import org.immutables.value.Value;

@Value.Immutable
public abstract class MainUiModel {

    public abstract int incrementValue();
}
