package example.com.redux;

import org.immutables.value.Value;

@Value.Enclosing
abstract class MainUiEventActions implements UiEventActions<MainUiModel> {

    public static class IncrementValue extends MainUiEventActions {
        @Override
        @Value.Derived
        public MainUiModel getState(MainUiModel oldState) {
            return ImmutableMainUiModel.copyOf(oldState).withIncrementValue(oldState.incrementValue() + 1);
        }
    }

    public static class IncrementValueSuccess extends MainUiEventActions {
        @Override
        @Value.Derived
        public MainUiModel getState(MainUiModel oldState) {
            return oldState;
        }
    }
}
