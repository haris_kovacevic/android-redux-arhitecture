package example.com.redux;


import android.arch.lifecycle.ViewModel;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

public abstract class ReduxViewModel<UiEvents, UiEventActions, UiModel> extends ViewModel {
    protected PublishSubject<UiEvents> mUiEvents = PublishSubject.create();
    protected BehaviorSubject<UiModel> mUiModel = BehaviorSubject.create();
    protected Disposable mSubscription;


    public ReduxViewModel() {
        mSubscription = createStream().distinctUntilChanged().subscribe(this::setState);
    }

    private Flowable<UiModel> createStream() {
        return transform().scan(getInitialState(), this::reduce).toFlowable(BackpressureStrategy.LATEST);
    }

    protected abstract UiModel reduce(UiModel oldState, UiEventActions event);

    protected abstract Observable<UiEventActions> transform();

    protected abstract UiModel getInitialState();


    private void setState(UiModel mainUiModel) {
        mUiModel.onNext(mainUiModel);
    }

    BehaviorSubject<UiModel> getUiModel() {
        return mUiModel;
    }

    void postEvent(UiEvents event) {
        mUiEvents.onNext(event);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mSubscription.dispose();
    }
}
