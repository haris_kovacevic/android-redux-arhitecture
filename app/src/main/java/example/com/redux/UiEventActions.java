package example.com.redux;


public interface UiEventActions<State> {
    MainUiModel getState(State oldState);
}
