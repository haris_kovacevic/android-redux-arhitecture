package example.com.redux;

import org.immutables.value.Value;

@Value.Enclosing
public class MainUIEvents {

    public static class OnIncrementClicked extends MainUIEvents {
    }

    public static class OnIncrementValueClickExecuted extends MainUIEvents {
    }
}
