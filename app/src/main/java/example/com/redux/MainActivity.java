package example.com.redux;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import example.com.redux.databinding.ActivityMainBinding;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.BehaviorSubject;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding mBinding;
    private MainViewModel mViewModel;
    private CompositeDisposable mSubscriptions = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        setupUI();
        handleUiModelSubscription(mViewModel.getUiModel());
    }

    private void setupUI() {
        mBinding.increment.setOnClickListener(x -> mViewModel.postEvent(new MainUIEvents.OnIncrementClicked()));
    }

    private void handleUiModelSubscription(BehaviorSubject<MainUiModel> uiModel) {
        mSubscriptions.addAll(
                uiModel.map(MainUiModel::incrementValue).distinctUntilChanged().subscribe(this::incrementValue)
        );
    }

    private void incrementValue(int value) {
        mBinding.value.setText(String.valueOf(value));
        mViewModel.postEvent(new MainUIEvents.OnIncrementValueClickExecuted());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSubscriptions.dispose();
    }
}
