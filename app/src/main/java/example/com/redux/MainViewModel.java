package example.com.redux;

import io.reactivex.Observable;


public class MainViewModel extends ReduxViewModel<MainUIEvents, MainUiEventActions, MainUiModel> {

    @Override
    protected MainUiModel reduce(MainUiModel oldState, MainUiEventActions event) {
        return event.getState(oldState);
    }

    @Override
    protected Observable<MainUiEventActions> transform() {
        return Observable.merge(
                mUiEvents.ofType(MainUIEvents.OnIncrementClicked.class).map(x -> new MainUiEventActions.IncrementValue()),
                mUiEvents.ofType(MainUIEvents.OnIncrementValueClickExecuted.class).map(x -> new MainUiEventActions.IncrementValueSuccess())

        );
    }

    @Override
    protected MainUiModel getInitialState() {
        return ImmutableMainUiModel.builder()
                .incrementValue(0)
                .build();
    }


}
